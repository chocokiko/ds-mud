# console input processing
import cmd, readline

# game modules
from Entity import Player
from Map import GameMap

class CmdProcessor(cmd.Cmd):
    """Simple command processor example."""

    prompt = '> '
    use_rawinput = False

    def __init__(self):
        cmd.Cmd.__init__(self)
        self.player = Player()
        self.game_map = GameMap()

    def do_greet(self, person):
        """
            greet [person] Greet the named person
            example method for testing - remove later
        """
        if person:
            print "hi,", person
        else:
            print 'hi'

    def do_north(self, line):
        '''
            Go north (up)
        '''
        self.player.move(0, -1)
        self.game_map.render(self.player)
        print 'You went north.'

    def do_n(self, line):
        '''
            Go north (up)
        '''
        self.do_north(line)

    def do_up(self, line):
        '''
            Go north (up)
        '''
        self.do_north(line)

    def do_south(self, line):
        '''
            Go south (down)
        '''
        self.player.move(0, 1)
        self.game_map.render(self.player)
        print 'You went south.'

    def do_s(self, line):
        '''
            Go south (down)
        '''
        self.do_south(line)

    def do_down(self, line):
        '''
            Go south (down)
        '''
        self.do_south(line)

    def do_east(self, line):
        '''
            Go east (left)
        '''
        self.player.move(1, 0)
        self.game_map.render(self.player)
        print 'You went east.'

    def do_e(line):
        '''
            Go east (left)
        '''
        self.do_east(line)

    def do_left(line):
        '''
            Go east (left)
        '''
        self.do_east(line)

    def do_west(self, line):
        '''
            Go west (right)
        '''
        self.player.move(-1, 0)
        self.game_map.render(self.player)
        print 'You went west.'

    def do_w(self, line):
        '''
            Go west (right)
        '''
        self.do_west(line)

    def do_right(self, line):
        '''
            Go west (right)
        '''
        self.do_west(line)

    def do_EOF(self, line):
        '''
            leave program by CTRL+D
        '''
        return True

    def do_quit(self, line):
        '''
            quit program
        '''
        return True

    def do_exit(self, line):
        '''
            quit program
        '''
        return True

    def postloop(self):
        print

