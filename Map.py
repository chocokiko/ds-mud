import sys

class GameMap(object):

    """GameMap that shows where the player is."""

    DUNGON_LAYER = [
        '#####################',
        '#...................#',
        '#...................#',
        '##########.##########',
        '#........#..........#',
        '#........#..........#',
        '#........#..........#',
        '#........#..........#',
        '#...................#',
        '#####################'
    ]

    ITEM_LAYER = [
        '.....................',
        '.....................',
        '.................%...',
        '.....................',
        '.....................',
        '.....................',
        '...%.................',
        '.....................',
        '.....................',
        '.....................',
    ]

    ENTITY_LAYER = None


    def render(self, player):
        """Render map and player position."""

        y = 0
        for row in self.DUNGON_LAYER:
            y = y + 1
            x = 0
            for tile in row:
                x = x + 1
                if (player.x == x) and (player.y == y):
                    sys.stdout.write('@'),
                else:
                    sys.stdout.write(tile),
            print '  ', player.x, player.y, y


