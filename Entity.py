'''

'''

class Entity(object):
    '''

    '''
    def __init__(self):
        # Attributes
        self.body = 0
        self.agility = 0
        self.mind = 0

        # Abilities
        self.strength = 0
        self.toughness = 0
        self.reflexes = 0
        self.dexterity = 0
        self.reason = 0
        self.aura = 0

        # stats
        self.hitpoints = 0

        self.level = 0
        self.experience = 0
        self.weight = 0
        self.bulk = 0

        #equipment slots
        # @see castle of the winds
        self.armor = None
        self.neckwear = None
        self.overgarment= None
        self.helmet = None
        self.bracers = None
        self.gauntlets = None
        self.weapon = None
        self.freehand = None
        self.rightring = None
        self.leftring = None
        self.belt = None
        self.boots = None
        self.pack = None
        self.purse= None

    # standard settters/getters
    def setBody(body):
        self.body = body

    def getBody():
        return self.body

    def setAgility(agility):
        self.agility = agility

    def getAgility():
        return self.agility

    # TODO: other standard getters/setters

    # shorthand getters/setters
    def bod(body=None):
        # if None get else set
        pass

    # unspecific getter/s setters
    def setAttribute(attribute, value):
        pass

    def setAbility(ability, value):
        pass

    # compound getters/setters
    def setAttributes(bod, agi, min):
        pass

    # combat value getters
    def getMaxHitpoints():
        pass



class Player(Entity):
    '''
        Player object with some actions (just move for now).
    '''
    def __init__(self):
        """
            Initialize coordinates.
        """
        Entity.__init__(self)
        self.x = 3
        self.y = 3

    def move(self, dx, dy):
        """
            Move player to some position.

            :param dx: How much to move in the x axis
            :type dx: int
            :param dy: How much to move in the y axis
            :type dy: int
        """
        # Ignore invalid movements
        if (self.x + dx) < 1 or (self.y + dy) < 1:
            return

        self.x += dx
        self.y += dy


# class Monster(Entity):
#     '''

#     '''
#     def __init__(self):
#         Entity.__init__(self)
#         pass

