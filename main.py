#!/usr/bin/env python
"""
SYNOPSIS

    TODO helloworld [-h,--help] [-v,--verbose] [--version]

DESCRIPTION

    simple roguelike/mud game that can be played via various network protocols e.g. XMPP, IRC, HTTP, Telnet, SSH, Email etc.
    Easy to attach clients to.

AUTHOR

    Chocokiko <name@example.org>

LICENSE

    This script is in the public domain, free from copyrights or restrictions.

VERSION

    $Id$


"""

# used for debugging remove later
import sys, os, traceback, optparse, time, re

# game modules
from Command import CmdProcessor


class Game(object):

    """Game object to keep state and read command for next turn."""

    def __init__(self, options, args):
        """Get all the objects needed to run the game."""
        self.command = CmdProcessor()

    def run(self):
        """Render map and read next command from stdin."""
        # while True:
        #     self.game_map.render(self.player)
        #     self.player.move(*self.command.read())

        self.command.cmdloop()


def main():
    global options, args

    # set up basic command line processor for action processing


if __name__ == '__main__':
    try:
        start_time = time.time()

        parser = optparse.OptionParser(formatter=optparse.TitledHelpFormatter(), usage=globals()['__doc__'], version='$Id$')
        parser.add_option ('-v', '--verbose', action='store_true', default=False, help='verbose output')

        (options, args) = parser.parse_args()

        #if len(args) < 1:
        #    parser.error ('missing argument')

        if options.verbose: print time.asctime()

        game = Game(options, args)
        game.run()

        if options.verbose: print time.asctime()
        if options.verbose: print 'TOTAL TIME IN MINUTES:',
        if options.verbose: print (time.time() - start_time) / 60.0

        sys.exit(0)

    except KeyboardInterrupt, e: # Ctrl-C
        raise e

    except SystemExit, e: # sys.exit()
        raise e

    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
